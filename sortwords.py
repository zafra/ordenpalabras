#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

"""Programa creado por Alberto García Zafra"""

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    f_string = first.lower()
    s_string = second.lower()

    if f_string < s_string:
        return True
    else:
        return False


def get_lower(words: list, pos: int) -> int:
    """Get lower word, for words right of pos (including pos)"""
    lower: int = pos
    for i in range(pos, len(words)):
        if is_lower(words[i], words[lower]):
            lower= i
    return lower



def sort(words: list) -> list:
    """Return the list of words, ordered alphabetically"""
    listademenores = []
    for pivot_pos in range(0, len(words)):
        lower_pos: int = get_lower(words, pivot_pos)
        listademenores.append(words[lower_pos])
        words[lower_pos] = words[pivot_pos]
    return listademenores



def show(words: list):
    """Show words on screen, using print()"""
    word = " ".join(words)
    showed=print(word)
    return showed


def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()

